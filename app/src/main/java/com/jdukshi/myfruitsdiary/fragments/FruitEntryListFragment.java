package com.jdukshi.myfruitsdiary.fragments;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jdukshi.myfruitsdiary.R;

import com.jdukshi.myfruitsdiary.adapters.FruitEntryListAdapter;
import com.jdukshi.myfruitsdiary.databinding.RecyclerviewListBinding;
import com.jdukshi.myfruitsdiary.db.DataBaseHelper;
import com.jdukshi.myfruitsdiary.model.FruitEntryList;
import com.jdukshi.myfruitsdiary.model.FruitList;
import com.jdukshi.myfruitsdiary.viewmodel.MainViewModel;


import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class FruitEntryListFragment extends Fragment {
    private MainViewModel mainViewModel;
    private FruitEntryListAdapter adapter;
    private int entryId;
    private int position;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        RecyclerviewListBinding recyclerviewBinding = DataBindingUtil.inflate(inflater, R.layout.recyclerview_list, container, false);
        View view = recyclerviewBinding.getRoot();
        RecyclerView recyclerView = recyclerviewBinding.viewEntryList;
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        setHasOptionsMenu(true);
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        adapter = new FruitEntryListAdapter();
        fruits();
        getEntry(position);
        recyclerView.setAdapter(adapter);
        setUpInsertButton(view);
        adapter.notifyDataSetChanged();
        return view;
    }

    public void getID(int entryid, int itemPosition) {
        entryId = entryid;
        position = itemPosition;
    }

    private void setUpInsertButton(View view) {

        FloatingActionButton floatingActionButton = view.findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener((View fab) -> {

            Toast.makeText(getContext(), "Insert Fruit", Toast.LENGTH_SHORT).show();
           //TODO insertFruits
       /*     Dialog dialog = new Dialog(getContext());
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.setContentView(R.layout.recycler_dialog);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setCancelable(true);
            dialog.show();

            RecyclerView rvTest = dialog.findViewById(R.id.viewFruits);
            rvTest.setHasFixedSize(true);
            rvTest.setLayoutManager(new LinearLayoutManager(getContext()));

            compositeDisposable.add(FruitList.getFruitListObservable(DataBaseHelper.getDatabase(getContext())).
                    observeOn(Schedulers.io()).subscribeOn(AndroidSchedulers.mainThread()).subscribe(
                    fruitLists ->  {
            DialogFragmentAdapter rvAdapter = new DialogFragmentAdapter(fruitLists,getContext());
            rvTest.setAdapter(rvAdapter);
                    },Throwable::printStackTrace

            ));*/


        });
    }

    public void fruits() {

        compositeDisposable.add(FruitList.getFruitListObservable(DataBaseHelper.getDatabase(getContext())).
                observeOn(Schedulers.io()).subscribeOn(Schedulers.io()).subscribe(
                fruitLists -> adapter.setFruits(fruitLists)
        ));


    }


    private void getEntry(int id) {
        mainViewModel.getSingleEntry(id).observe(this, entrylist -> adapter.setFruitEntryList((ArrayList<FruitEntryList>) entrylist));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        MenuItem item = menu.findItem(R.id.delete);
        if (item != null)
            item.setVisible(false);
    }
}