package com.jdukshi.myfruitsdiary.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.jdukshi.myfruitsdiary.BuildConfig;
import com.jdukshi.myfruitsdiary.R;
import com.jdukshi.myfruitsdiary.databinding.FragmentAboutBinding;

public class AboutFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentAboutBinding bindingaboutFragment = DataBindingUtil.inflate(inflater, R.layout.fragment_about,container,false);
        bindingaboutFragment.setTemp(BuildConfig.VERSION_NAME);
        setHasOptionsMenu(true);
        return  bindingaboutFragment.getRoot();
    }
    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        MenuItem item=menu.findItem(R.id.delete);
        if(item!=null)
            item.setVisible(false);
    }

}
