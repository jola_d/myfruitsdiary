package com.jdukshi.myfruitsdiary.fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jdukshi.myfruitsdiary.R;
import com.jdukshi.myfruitsdiary.adapters.FruitEntryAdapter;
import com.jdukshi.myfruitsdiary.databinding.RecyclerviewBinding;
import com.jdukshi.myfruitsdiary.db.DataBaseHelper;
import com.jdukshi.myfruitsdiary.model.Date;
import com.jdukshi.myfruitsdiary.model.FruitEntry;
import com.jdukshi.myfruitsdiary.model.FruitList;
import com.jdukshi.myfruitsdiary.viewmodel.MainViewModel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


public class FruitEntryFragment extends Fragment {

    private MainViewModel mainViewModel;
    private FruitEntryAdapter fruitEntryAdapter;
    private EntryOnClikListener mListener;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        RecyclerviewBinding recyclerviewBinding = DataBindingUtil.inflate(inflater, R.layout.recyclerview, container, false);
        View view = recyclerviewBinding.getRoot();
        RecyclerView recyclerView = recyclerviewBinding.viewEntries;
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        fruitEntryAdapter = new FruitEntryAdapter(mListener);
        updateFruits();
        recyclerView.setAdapter(fruitEntryAdapter);
        setUpInsertButton(view, getContext());
        setupSwipeToDelete(recyclerView);
        fruitEntryAdapter.notifyDataSetChanged();
        return view;

    }

    //adding entries to the list
    private void setUpInsertButton(View view, Context context) {
        FloatingActionButton floatingActionButton = view.findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener((View fab) -> {
            Calendar myCalendar = Calendar.getInstance();
            DatePickerDialog.OnDateSetListener onDateSetListener = (view1, year, monthOfYear, dayOfMonth) -> {
                myCalendar.set(year, monthOfYear, dayOfMonth);
                mainViewModel.addEntry(new Date(new SimpleDateFormat("yyyy-MM-dd").format(myCalendar.getTime())))
                        .observe(this, response -> {
                            if (response.code()==200) {
                                updateFruits();
                            } else {
                                Toast.makeText(getContext(),"Entry not ineserted. Check for duplicates", Toast.LENGTH_SHORT);
                            }

                        });
            };

            new DatePickerDialog(context, onDateSetListener, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        });
    }

    //swipe left to delete an entry
    private void setupSwipeToDelete(RecyclerView recyclerView) {
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
                final int position = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.LEFT) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage(R.string.confirm_delete);
                    builder.setPositiveButton(R.string.remove, (dialog, which) -> {
                        String entryId = String.valueOf(fruitEntryAdapter.getEntry(position).getId());
                        mainViewModel.deleteEntry(entryId)
                                .observe((LifecycleOwner) getContext(), response -> {
                                    if (response.code()==200) {
                                        fruitEntryAdapter.removeEntry(position);
                                    } else {
                                        Toast.makeText(getContext(),"Entry not removed",Toast.LENGTH_SHORT).show();
                                        fruitEntryAdapter.notifyDataSetChanged();
                                    }

                                });

                        return;
                    }).setNegativeButton(R.string.cancel, (dialog, which) -> {
                        fruitEntryAdapter.notifyDataSetChanged();
                        return;
                    }).show();
                }
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    private void updateFruits() {
        compositeDisposable.add(FruitList.getFruitListObservable(DataBaseHelper.getDatabase(getContext())).
                observeOn(Schedulers.io()).subscribeOn(Schedulers.io()).subscribe(
                fruitLists -> fruitEntryAdapter.setFruits(fruitLists)
        ));


        getAllEntries();
    }

    public void getAllEntries() {
        mainViewModel.getAllEntries().observe(this,
                entrylist -> fruitEntryAdapter.setFruitEntry((ArrayList<FruitEntry>) entrylist));
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof EntryOnClikListener) {
            mListener = (EntryOnClikListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement listener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }
}
