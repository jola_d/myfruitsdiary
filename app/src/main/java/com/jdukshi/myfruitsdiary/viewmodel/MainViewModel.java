package com.jdukshi.myfruitsdiary.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.jdukshi.myfruitsdiary.api.EntryRepository;
import com.jdukshi.myfruitsdiary.model.Date;
import com.jdukshi.myfruitsdiary.model.FruitEntry;
import com.jdukshi.myfruitsdiary.model.FruitEntryList;
import com.jdukshi.myfruitsdiary.model.FruitList;

import java.util.List;

import retrofit2.Response;

public class MainViewModel extends AndroidViewModel {

    public EntryRepository entryRepository;

    public MainViewModel(@NonNull Application application) {
        super(application);
        entryRepository = new EntryRepository(getApplication());
    }

    public LiveData<List<FruitEntry>> getAllEntries() {
        return entryRepository.getMutableLiveData();
    }

    public LiveData<List<FruitEntryList>> getSingleEntry(int id) {
        return entryRepository.getSingleEntry(id);
    }
    public LiveData<List<FruitList>> getFruits() {
        return entryRepository.getFruitList();
    }

    public LiveData<Response> addEntry(Date date) {
        return entryRepository.insertEntry(date);
    }
    public LiveData<Response> addFruit(String entryid,String fruitId,String amount) {
        return entryRepository.insertFruit(entryid,fruitId,amount);
    }

    public LiveData<Response>  deleteEntry(String entry) {
        return entryRepository.deleteEntry(entry);
    }

}
