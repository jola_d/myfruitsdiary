package com.jdukshi.myfruitsdiary.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiDataService {

    public static  Retrofit retrofit=null;
    public static final String BASE_URL="https://fruitdiary.test.themobilelife.com/";

    public static DataService getService() {
        if (retrofit == null) {
            retrofit = new Retrofit
                    .Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit.create(DataService.class);
    }

}
