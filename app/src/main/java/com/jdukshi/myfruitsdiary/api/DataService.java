package com.jdukshi.myfruitsdiary.api;

import com.jdukshi.myfruitsdiary.model.Date;
import com.jdukshi.myfruitsdiary.model.FruitEntry;
import com.jdukshi.myfruitsdiary.model.FruitList;

import java.util.ArrayList;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface DataService {

    @GET("api/fruit")
    Call<ArrayList<FruitList>> getFruits();
    @GET("api/entries")
    Call<ArrayList<FruitEntry>> getEntries();
    @POST("api/entries")
    Call<ResponseBody> insertNewEntry(@Body Date date);
    @POST("api/entry")
    Call<ResponseBody> insertNewFruit(
            @Field("entryId") String entryId,
            @Field("fruitId") String fruitId,
            @Field("nrOfFruit") String noOfFruits);

    @DELETE("api/entry/{entryId}")
    Call<ResponseBody> deleteEntryById(@Path("entryId") String id);
    @DELETE("api/entries")
    Call<ResponseBody> deleteAllEntries();


}
