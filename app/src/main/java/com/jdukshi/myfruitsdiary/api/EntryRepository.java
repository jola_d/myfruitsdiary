package com.jdukshi.myfruitsdiary.api;

import android.content.Context;
import android.widget.Toast;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.jdukshi.myfruitsdiary.adapters.FruitEntryAdapter;
import com.jdukshi.myfruitsdiary.db.DataBaseHelper;
import com.jdukshi.myfruitsdiary.model.Date;
import com.jdukshi.myfruitsdiary.model.FruitEntry;
import com.jdukshi.myfruitsdiary.model.FruitEntryList;
import com.jdukshi.myfruitsdiary.model.FruitList;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EntryRepository {

    public Context context;

    public EntryRepository(Context context) {
        this.context = context;
    }

    private ArrayList<FruitEntryList> entry = new ArrayList<>();
    private MutableLiveData<List<FruitEntry>> mutableLiveData = new MutableLiveData<>();

    public LiveData<List<FruitEntry>> getMutableLiveData() {
        final DataService dataService = ApiDataService.getService();

        Call<ArrayList<FruitEntry>> call = dataService.getEntries();
        call.enqueue(new Callback<ArrayList<FruitEntry>>() {
            @Override
            public void onResponse(Call<ArrayList<FruitEntry>> call, Response<ArrayList<FruitEntry>> response) {
                if (response.isSuccessful()) {
                    ArrayList<FruitEntry> fruitEntry = response.body();
                    mutableLiveData.setValue(fruitEntry);
                } else {
                    Toast.makeText(context, "Error: " + response.body(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<FruitEntry>> call, Throwable t) {
                Toast.makeText(context, "Error code Failure", Toast.LENGTH_SHORT).show();
            }
        });
        return mutableLiveData;
    }

    public LiveData<List<FruitEntryList>> getSingleEntry(int id) {
        MutableLiveData<List<FruitEntryList>> liveData = new MutableLiveData<>();
        final DataService dataService = ApiDataService.getService();

        Call<ArrayList<FruitEntry>> call = dataService.getEntries();
        call.enqueue(new Callback<ArrayList<FruitEntry>>() {
            @Override
            public void onResponse(Call<ArrayList<FruitEntry>> call, Response<ArrayList<FruitEntry>> response) {
                if (response.isSuccessful()) {
                    ArrayList<FruitEntryList> fruitEntry = response.body().get(id).getFruitEntryList();
                    liveData.setValue(fruitEntry);
                } else {
                    Toast.makeText(context, "Error: " + response.body(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<FruitEntry>> call, Throwable t) {
                Toast.makeText(context, "Error code Failure", Toast.LENGTH_SHORT).show();
            }
        });
        return liveData;
    }

    public LiveData<List<FruitList>> getFruitList() {
        MutableLiveData<List<FruitList>> liveData = new MutableLiveData<>();
        final DataService dataService = ApiDataService.getService();

        Call<ArrayList<FruitList>> call = dataService.getFruits();
        call.enqueue(new Callback<ArrayList<FruitList>>() {
            @Override
            public void onResponse(Call<ArrayList<FruitList>> call, Response<ArrayList<FruitList>> response) {
                if (response.isSuccessful()) {
                    ArrayList<FruitList> fruitEntry = response.body();
                    liveData.setValue(fruitEntry);
                } else {
                    Toast.makeText(context, "Error: " + response.body(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<FruitList>> call, Throwable t) {
                Toast.makeText(context, "Error code Failure", Toast.LENGTH_SHORT).show();
            }
        });
        return liveData;
    }
  /*  public LiveData<List<FruitEntry>> addEntry(FruitEntry entry) {
        MutableLiveData<List<FruitEntry>> liveData = new MutableLiveData<>();
        final DataService dataService = ApiDataService.getService();

        Call<ArrayList<FruitEntry>> call = dataService.getEntries();
        call.enqueue(new Callback<ArrayList<FruitEntry>>() {
            @Override
            public void onResponse(Call<ArrayList<FruitEntry>> call, Response<ArrayList<FruitEntry>> response) {
                if (response.isSuccessful()) {
                    ArrayList<FruitEntryList> fruitEntry = response.body().get(id).getFruitEntryList();
                    liveData.setValue(fruitEntry);
                } else {
                    Toast.makeText(context, "Error: " + response.body(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<FruitEntry>> call, Throwable t) {
                Toast.makeText(context, "Error code Failure", Toast.LENGTH_SHORT).show();
            }
        });
        return liveData;
    }*/

    public LiveData<Response> insertEntry(Date date) {
        final DataService dataService = ApiDataService.getService();

        MutableLiveData<Response> liveData = new MutableLiveData<>();
        dataService.insertNewEntry(date).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()) {
                    liveData.setValue(response);
                } else Toast.makeText(context,"Something went wrong"+response.message(),Toast.LENGTH_SHORT);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context,"Something went wrong",Toast.LENGTH_SHORT);
            }
        });
        return liveData;

    }
    public LiveData<Response> insertFruit(String entryid,String fruitId,String amount) {
        final DataService dataService = ApiDataService.getService();

        MutableLiveData<Response> liveData = new MutableLiveData<>();
        dataService.insertNewFruit(entryid,fruitId,amount).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()) {
                    liveData.setValue(response);
                }else Toast.makeText(context,"Something went wrong"+response.message(),Toast.LENGTH_SHORT);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context,"Something went wrong",Toast.LENGTH_SHORT);
            }
        });
        return liveData;

    }


    public LiveData<Response>  deleteEntry(String id) {
        final DataService dataService = ApiDataService.getService();

        MutableLiveData<Response> liveData = new MutableLiveData<>();
        dataService.deleteEntryById(id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                liveData.setValue(response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
        return liveData;
    }


}
