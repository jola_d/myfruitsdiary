package com.jdukshi.myfruitsdiary.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.jdukshi.myfruitsdiary.model.FruitList;

public class DataBaseHelper extends SQLiteOpenHelper {

    private static DataBaseHelper mHelper;
    private static String mDbName;
    private static Context mContext;
    private static final Object mLock = new Object();
    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "MyFruitDiary";

    private static final String[] SQL_TABLES =
            {
                    FruitList.TABLE_NAME
            };

    private static final String[] SQL_COLUMNS_WITH_TYPES = {
            FruitList.getColumnsWithType()
    };
    private static final String[] SQL_COLUMNS = {
            FruitList.getColumns()
    };


    public DataBaseHelper(Context context, String dbName) {
        super(context, dbName, null, DATABASE_VERSION);
        mContext = context;
        mDbName = dbName;
    }

    public static SQLiteDatabase getDatabase(Context context) {

        return getInstance(context.getApplicationContext(), DATABASE_NAME).getWritableDatabase();
    }

    public static DataBaseHelper getInstance(Context context, String dbName) {
        synchronized (mLock) {
            if (mHelper == null || context != mContext || !dbName.equals(mDbName)) {
                mHelper = new DataBaseHelper(context.getApplicationContext(), dbName);
            }
            return mHelper;
        }
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try {
            sqLiteDatabase.beginTransaction();

            for (int i = 0; i < SQL_TABLES.length; i++) {

                sqLiteDatabase.execSQL("CREATE TABLE " + SQL_TABLES[i] + " ( " + SQL_COLUMNS_WITH_TYPES[i] + " );");
            }
            sqLiteDatabase.setTransactionSuccessful();
        } catch (Exception exc) {
            exc.printStackTrace();
        } finally {
            sqLiteDatabase.endTransaction();

        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        try {
            sqLiteDatabase.beginTransaction();
            for (String table : SQL_TABLES) {
                sqLiteDatabase.execSQL("DROP TABLE IF EXISTS '" + SQL_TABLES + table + "'");
            }

            sqLiteDatabase.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            sqLiteDatabase.endTransaction();

        }
        // create new tables
        onCreate(sqLiteDatabase);
    }
}
