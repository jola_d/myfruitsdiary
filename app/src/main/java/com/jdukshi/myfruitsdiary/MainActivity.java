package com.jdukshi.myfruitsdiary;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.jdukshi.myfruitsdiary.api.ApiDataService;
import com.jdukshi.myfruitsdiary.api.DataService;
import com.jdukshi.myfruitsdiary.databinding.ActivityMainBinding;
import com.jdukshi.myfruitsdiary.db.DataBaseHelper;
import com.jdukshi.myfruitsdiary.fragments.AboutFragment;
import com.jdukshi.myfruitsdiary.fragments.EntryOnClikListener;
import com.jdukshi.myfruitsdiary.fragments.FruitEntryFragment;
import com.jdukshi.myfruitsdiary.fragments.FruitEntryListFragment;
import com.jdukshi.myfruitsdiary.fragments.OnFruitClickListener;
import com.jdukshi.myfruitsdiary.model.FruitList;
import com.jdukshi.myfruitsdiary.viewmodel.MainViewModel;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements EntryOnClikListener {
    FruitEntryFragment fruitEntryFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataBindingUtil.setContentView(this, R.layout.activity_main);
        fruitEntryFragment = new FruitEntryFragment();
        retriveFruits();
        getSupportFragmentManager().beginTransaction().add(R.id.frame, fruitEntryFragment).addToBackStack(null).commit();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        if (fruitEntryFragment == null) {
            menu.findItem(R.id.delete).setVisible(true);

        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about_menu:
                AboutFragment aboutFragment = new AboutFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame, aboutFragment).addToBackStack(null).commit();
                break;
            case R.id.delete:

                deleteAllEntries();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    public void deleteAllEntries() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.confirm_delete_all);
        builder.setPositiveButton(R.string.remove, (dialog, which) -> {
            final DataService dataService = ApiDataService.getService();
            Call<ResponseBody> call = dataService.deleteAllEntries();
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        if (fruitEntryFragment != null) {
                            fruitEntryFragment.getAllEntries();
                        }
                    } else
                        Toast.makeText(getBaseContext(), "Something went wrong, entries are not deleted!", Toast.LENGTH_SHORT);

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
            return;
        }).setNegativeButton(R.string.cancel, (dialog, which) -> {
            dialog.dismiss();
        }).show();

    }

    public void retriveFruits() {
        final DataService dataService = ApiDataService.getService();
        Call<ArrayList<FruitList>> call = dataService.getFruits();
        call.enqueue(new Callback<ArrayList<FruitList>>() {
            @Override
            public void onResponse(Call<ArrayList<FruitList>> call, Response<ArrayList<FruitList>> response) {
                if (response.isSuccessful()) {
                    ArrayList<FruitList> list = response.body();
                    for (FruitList fruitList : list) {
                        fruitList.save(DataBaseHelper.getDatabase(getApplicationContext()));
                    }
                } else Toast.makeText(getBaseContext(), "Something went wrong", Toast.LENGTH_SHORT);

            }

            @Override
            public void onFailure(Call<ArrayList<FruitList>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(int id, int position) {
        FruitEntryListFragment fruitEntryListFragment = new FruitEntryListFragment();
        fruitEntryListFragment.getID(id, position);
        getSupportFragmentManager().beginTransaction().replace(R.id.frame, fruitEntryListFragment)
                .addToBackStack(null).commit();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
