package com.jdukshi.myfruitsdiary.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.jdukshi.myfruitsdiary.R;
import com.jdukshi.myfruitsdiary.databinding.FragmentFruitEntryBinding;
import com.jdukshi.myfruitsdiary.fragments.EntryOnClikListener;
import com.jdukshi.myfruitsdiary.model.FruitEntry;
import com.jdukshi.myfruitsdiary.model.FruitList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FruitEntryAdapter extends RecyclerView.Adapter<FruitEntryAdapter.FruitEntryViewHolder>{

    private final EntryOnClikListener mListener;
    private Map<Integer, FruitList> fruits = new HashMap<>();
    private ArrayList<FruitEntry> fruitEntry;

    public FruitEntryAdapter(EntryOnClikListener listener) {
        mListener = listener;

    }

    @NonNull
    @Override
    public FruitEntryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        FragmentFruitEntryBinding fruitEntryListItemBinding =
                DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                        R.layout.fragment_fruit_entry, viewGroup, false);
        return new FruitEntryViewHolder(fruitEntryListItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull FruitEntryViewHolder fruitEntryViewHolder, int i) {
        FruitEntry entry = fruitEntry.get(i);
        entry.setTotalFruits(
                entry.getFruitEntryList().stream()
                        .mapToInt(entryfruit -> Integer.parseInt(entryfruit.getAmount())).sum());
        entry.setPosition(i);
        entry.setTotalVitamins(entry.getFruitEntryList().stream()
                .mapToInt(entryFruit ->
                        Integer.parseInt(fruits.get(entryFruit.getFruitId()).getVitamins()) * Integer.parseInt(entryFruit.getAmount()))
                .sum());

        fruitEntryViewHolder.bindFruitEntries(entry);
    }

    @Override
    public int getItemCount() {
        if (fruitEntry != null) {
            return fruitEntry.size();
        } else {
            return 0;
        }
    }

    public void setFruits(List<FruitList> fruitsList) {
        fruitsList.stream().forEach(fruit -> fruits.put(fruit.getId(), fruit));
    }
    public FruitEntry getEntry(int position) {
        return fruitEntry.get(position);
    }
    public void removeEntry(int position) {
        fruitEntry.remove(position);
        notifyItemRemoved(position);
    }

    public void setFruitEntry(ArrayList<FruitEntry> entry) {
        this.fruitEntry = entry;
        notifyDataSetChanged();
    }


    class FruitEntryViewHolder extends RecyclerView.ViewHolder {
        private FragmentFruitEntryBinding fruitEntryItemBinding;

        public FruitEntryViewHolder(@NonNull FragmentFruitEntryBinding fruitEntryItemBinding) {
            super(fruitEntryItemBinding.getRoot());
            this.fruitEntryItemBinding = fruitEntryItemBinding;

        }

        void bindFruitEntries(FruitEntry entry) {
            fruitEntryItemBinding.setFruitEntry(entry);
            fruitEntryItemBinding.setClickListener((id,position) -> {
                if (null != mListener) {
                    mListener.onClick(fruitEntryItemBinding.getFruitEntry().getId(),position);
                }

            });

        fruitEntryItemBinding.executePendingBindings();

        }

    }

}
