package com.jdukshi.myfruitsdiary.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.jdukshi.myfruitsdiary.R;
import com.jdukshi.myfruitsdiary.databinding.FragmentFruitEntryListBinding;
import com.jdukshi.myfruitsdiary.model.FruitEntryList;
import com.jdukshi.myfruitsdiary.model.FruitList;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FruitEntryListAdapter extends RecyclerView.Adapter<FruitEntryListAdapter.FruitEntryListViewHolder>{

    private ArrayList<FruitEntryList> fruitEntryLists;
    private Map<Integer, FruitList> fruits = new HashMap<>();
    @NonNull
    @Override
    public FruitEntryListAdapter.FruitEntryListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        FragmentFruitEntryListBinding fruitEntryListItemBinding =
                DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                        R.layout.fragment_fruit_entry_list, viewGroup, false);
        return new FruitEntryListAdapter.FruitEntryListViewHolder(fruitEntryListItemBinding);
    }
    @Override
    public void onBindViewHolder(@NonNull FruitEntryListAdapter.FruitEntryListViewHolder fruitEntryViewHolder, int i) {
        FruitEntryList entryList = fruitEntryLists.get(i);
        entryList.setVitamins(fruits.get(entryList.getFruitId()).getVitamins());
        entryList.setImage(fruits.get(entryList.getFruitId()).getImage());
        fruitEntryViewHolder.bindFruitLsit(entryList);

    }
    public void setFruits(List<FruitList> fruitsList) {
        fruitsList.stream().forEach(fruit -> fruits.put(fruit.getId(), fruit));
    }
    @Override
    public int getItemCount() {
        if (fruitEntryLists != null) {
            return fruitEntryLists.size();
        } else {
            return 0;
        }
    }
    public void setFruitEntryList(ArrayList<FruitEntryList> entry) {
        this.fruitEntryLists = entry;
        notifyDataSetChanged();
    }
    class FruitEntryListViewHolder extends RecyclerView.ViewHolder {
        private FragmentFruitEntryListBinding fruitEntryListItemBinding;
        public FruitEntryListViewHolder(@NonNull FragmentFruitEntryListBinding fruitEntryListItemBinding) {
            super(fruitEntryListItemBinding.getRoot());
            this.fruitEntryListItemBinding = fruitEntryListItemBinding;
        }
        void bindFruitLsit(FruitEntryList fruitEntryList){
            fruitEntryListItemBinding.setFruitEntryList(fruitEntryList);
            fruitEntryListItemBinding.executePendingBindings();

        }
    }


}
