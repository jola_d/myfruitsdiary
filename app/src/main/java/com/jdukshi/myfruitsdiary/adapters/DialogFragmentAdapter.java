package com.jdukshi.myfruitsdiary.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RadioButton;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.jdukshi.myfruitsdiary.R;
import com.jdukshi.myfruitsdiary.databinding.DialogFragmentBinding;
import com.jdukshi.myfruitsdiary.model.FruitList;
import java.util.ArrayList;

//dialogAdpater to show the list of fruits to be inserted
public class DialogFragmentAdapter extends RecyclerView.Adapter<DialogFragmentAdapter.DialogViewHolder> {

    private ArrayList<FruitList> fruitList;
    private Context context;
    public int mSelectedItem = -1;
    RadioButton button;

    public DialogFragmentAdapter(ArrayList<FruitList> list, Context context) {
        this.fruitList = list;
        this.context = context;

    }


    @NonNull
    @Override
    public DialogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        DialogFragmentBinding dialogFragmentBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.dialog_fragment, parent, false);
        return new DialogViewHolder(dialogFragmentBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DialogViewHolder holder, int position) {
        FruitList fruits = fruitList.get(position);
        fruits.setPosition(position);

        holder.bindData(fruits);

    }

    @Override
    public int getItemCount() {
        return fruitList.size();
    }

    class DialogViewHolder extends RecyclerView.ViewHolder {
        DialogFragmentBinding dialogFragmentBinding;


        public DialogViewHolder(DialogFragmentBinding dialogFragmentBinding) {
            super(dialogFragmentBinding.getRoot());
            this.dialogFragmentBinding = dialogFragmentBinding;

        }

        void bindData(FruitList fruitList) {
            dialogFragmentBinding.setFruitList(fruitList);
            button = dialogFragmentBinding.getRoot().findViewById(R.id.fruit_select);
            button.setOnClickListener(view -> {

            });
        }
    }
}
