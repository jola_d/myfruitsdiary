package com.jdukshi.myfruitsdiary.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.google.gson.annotations.SerializedName;
import com.jdukshi.myfruitsdiary.R;
import com.jdukshi.myfruitsdiary.api.ApiDataService;
import com.jdukshi.myfruitsdiary.api.EntryRepository;
import com.squareup.picasso.Picasso;

public class FruitEntryList implements Parcelable {

    @SerializedName("fruitId")
    private int fruitId;
    @SerializedName("fruitType")
    private String fruitType;
    @SerializedName("amount")
    private String amount;
    private String vitamins;
    private String image;

    private int entryId;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    public String getVitamins() {
        return vitamins;
    }

    public void setVitamins(String vitamins) {
        this.vitamins = vitamins;
    }

    public int getFruitId() {
        return fruitId;
    }

    public void setFruitId(int fruitId) {
        this.fruitId = fruitId;
    }

    public String getFruitType() {
        return fruitType;
    }

    public void setFruitType(String fruitType) {
        this.fruitType = fruitType;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    // important code for loading image here
    @BindingAdapter({ "listImage" })
    public static void loadImage(ImageView imageView, String imageURL) {
        Picasso.with(imageView.getContext())
                .load(ApiDataService.BASE_URL+imageURL)
                .placeholder(R.drawable.ic_reload)
                .into(imageView);


    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }
}
