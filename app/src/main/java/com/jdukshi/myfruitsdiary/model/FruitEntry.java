package com.jdukshi.myfruitsdiary.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FruitEntry {

    @SerializedName("id")
    private int id;
    @SerializedName("date")
    private String date;

    @SerializedName("fruit")
    private ArrayList<FruitEntryList> fruitEntryList;

    private int totalFruits;

    private int totalVitamins;

    private int position;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getTotalVitamins() {
        return totalVitamins;
    }

    public void setTotalVitamins(int totalVitamins) {
        this.totalVitamins = totalVitamins;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<FruitEntryList> getFruitEntryList() {
        return fruitEntryList;
    }

    public void setFruitEntryList(ArrayList<FruitEntryList> fruitEntryList) {
        this.fruitEntryList = fruitEntryList;
    }

    public int getTotalFruits() {
        return totalFruits;
    }

    public void setTotalFruits(int totalFruits) {
        this.totalFruits = totalFruits;
    }



}
