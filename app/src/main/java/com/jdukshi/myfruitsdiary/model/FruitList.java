package com.jdukshi.myfruitsdiary.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.strictmode.SqliteObjectLeakedViolation;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;


import androidx.core.util.Pair;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;

import com.google.gson.annotations.SerializedName;
import com.jdukshi.myfruitsdiary.R;

import java.util.ArrayList;
import java.util.Objects;

import io.reactivex.Observable;

public class FruitList {

    public static String TABLE_NAME = "FruitList";
    public static String COL_ID = "Id";
    public static String COL_TYPE = "Type";
    public static String COL_VITAMINS = "Vitamins";
    public static String COL_IMAGE = "Image";
    //getters and setters
    @SerializedName("id")
    private int id;
    @SerializedName("type")
    private String type;
    @SerializedName("vitamins")
    private String vitamins;
    @SerializedName("image")
    private String image;

    private int position;

    public ObservableField<String> amount = new ObservableField<>();

    public ObservableField<String> getAmount() {
        return amount;
    }

    public void setAmount(ObservableField<String> amount) {
        this.amount = amount;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVitamins() {
        return vitamins;
    }

    public void setVitamins(String vitamins) {
        this.vitamins = vitamins;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }




    public void save(SQLiteDatabase db) {
        if (db.update(TABLE_NAME, getContentValues(), COL_ID + " =? ", new String[]{getId() + ""}) == 0)
            db.insert(TABLE_NAME, COL_ID, getContentValues());
    }

    public static Observable<ArrayList<FruitList>> getFruitListObservable(SQLiteDatabase db) {
        String sql = "SELECT * FROM " + TABLE_NAME ;
        Cursor c = db.rawQuery(sql, new String[]{});
        ArrayList<FruitList> list = new ArrayList<>();
        while (c.moveToNext()) {
            list.add(readCurrentItem(c));
        }
        c.close();
        return Observable.just(list);


    }

    public static FruitList readCurrentItem(Cursor cursor) {
        int id = cursor.getColumnIndex(COL_ID);
        int type = cursor.getColumnIndex(COL_TYPE);
        int vitamins = cursor.getColumnIndex(COL_VITAMINS);
        int image = cursor.getColumnIndex(COL_IMAGE);

        FruitList fruit = new FruitList();
        fruit.setId(cursor.getInt(id));
        fruit.setType(cursor.getString(type));
        fruit.setVitamins(cursor.getString(vitamins));
        fruit.setImage(cursor.getString(image));

        return fruit;

    }

    private ContentValues getContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(COL_ID, id);
        cv.put(COL_TYPE, type);
        cv.put(COL_VITAMINS, vitamins);
        cv.put(COL_IMAGE, image);
        return cv;
    }


    //database integeration
    public static String getColumnsWithType() {
        return COL_ID + " INTEGER," +
                COL_TYPE + " VARCHAR," +
                COL_VITAMINS + " VARCHAR," +
                COL_IMAGE + " VARCHAR";

    }

    public static String getColumns() {
        return COL_ID + "," +
                COL_TYPE + "," +
                COL_VITAMINS + "," +
                COL_IMAGE + ",";
    }


}
